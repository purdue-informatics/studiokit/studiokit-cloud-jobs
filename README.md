﻿# StudioKit.Cloud.Jobs

**Note**: Requires the following peer dependencies in the Solution.
* **StudioKit.Data**
* **StudioKit.Data.Entity**
* **StudioKit.Encryption**
* **StudioKit.ErrorHandling.Interfaces**
* **StudioKit.ErrorHandling**
