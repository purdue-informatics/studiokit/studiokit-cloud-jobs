﻿using System;

namespace StudioKit.Cloud.Jobs.Exceptions
{
	public class JobException : Exception
	{
		public JobException()
		{
		}

		public JobException(string message)
			: base(message) { }

		public JobException(string message, Exception inner)
			: base(message, inner) { }
	}
}